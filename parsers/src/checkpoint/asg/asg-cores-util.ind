#! META
name: chkp-asg-cores-util
description: Retrieve CPU usage
type: monitoring
monitoring_interval: 5 minute
includes_resource_data: true
requires:
    vendor: checkpoint
    asg: true

#! COMMENTS
memory-free-kbytes:
    skip-documentation: true

memory-total-kbytes:
    skip-documentation: true
	
memory-usage:
    skip-documentation: true

disk-usage-percentage:
    skip-documentation: true

disk-used-kbytes:
    skip-documentation: true
	
disk-total-kbytes:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 asg_cores_util

#! PARSER::AWK

#|CPU \ Blade|2_3 |2_4 |
/CPU \\ Blade/ {
	split($0, tmpColumnsArr, "\\|")
	
	# Cleaning the array from empty entries that occur due to the split
	i = 1
	for (id in tmpColumnsArr) {
		if (tmpColumnsArr[id] != "") {
			columnsArr[i] = tmpColumnsArr[id]
			i++
		}
	}
}

#|cpu0       |29% |2%  |
/cpu[0-9]/ {
	cpu = $1
	gsub(/\|/, "", cpu)
	
	split($0, tmpUsageArr, "\\|")
	
	# Cleaning the array from empty entries that occur due to the split
	i = 1
	for (id in tmpUsageArr) {
		if (tmpUsageArr[id] != "") {
			usageArr[i] = tmpUsageArr[id]
			i++
		}
	}
	
	cpuTags["resource-metric"] = "true"
	
	i = 2
	while (i <= arraylen(columnsArr)) {	
		bladeChassis = columnsArr[i]
		split(columnsArr[i],bladeArr,"_")
		chassis = bladeArr[1]
		blade = bladeArr[2]
		usage = usageArr[i]
		gsub(/%/, "", usage)
		cpuTags["name"] = cpu " - chassis: " chassis " blade: " blade
		writeDoubleMetricWithLiveConfig("cpu-usage", cpuTags, "gauge", "300", usage, "CPU", "percentage", "cpu-id")
		i++
	}
}