#! META
name: fw-ctl-get-int
description: fw ctl get int for multiple kernel parameters
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-firewall: true
    os.name:
        neq: gaia-embedded   # intentional per IKP-932

#! COMMENTS
firewall-kparam:
    why: |
        Get interesting kernel parameters from the device.
    how: |
        By using the Check Point built-in "fw ctl get in" command, kernel parameter values are retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing RADIUS servers is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 fw ctl get int fwha_monitor_all_vlan
${nice-path} -n 15 fw ctl get int fwha_mac_magic

#! PARSER::AWK

############
# Why: Get information from different interesting kernel parameters.
# How: Using "fw ctl get"
###########

# fwha_monitor_all_vlan = 0
/=/ {
    paramtags["name"] = $1
    writeDoubleMetricWithLiveConfig("firewall-kparam", paramtags, "gauge", "60", $NF, "Kernel Parameters (selected)", "number", "name")
}
