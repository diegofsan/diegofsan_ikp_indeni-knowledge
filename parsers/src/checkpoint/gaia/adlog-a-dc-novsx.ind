#! META
name: chkp-gaia-adlog-a-dc-novsx
description: check status of connected domain controllers for Identity Awareness
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: checkpoint
    os.name: gaia

#! COMMENTS
identity-integration-connection-state:
    why: |
        When using Identity Awareness it is important to make sure that the domain controllers are connected, otherwise no new events will be retrieved.
    how: |
        Using the Check Point command "adlog a dc" we retreive the status of the domain controllers.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing status of domain controllers is only available from the command line interface or SmartDashboard.

#! REMOTE::SSH
${nice-path} -n 15 adlog a dc && sleep 5 && ${nice-path} -n 15 adlog a dc && sleep 5 && ${nice-path} -n 15 adlog a dc

#! PARSER::AWK

BEGIN {
	# The input is separated on many spaces.
	FS = "[ ]{3,}"
}

#Domain Name               IP Address                Events (last hour)   Connection state
/(Domain Name|IP Address|Events|Connection state)/ {
	# Parse the line into a column array.
	getColumns(trim($0), "[ ]{3,}", columns)
}


#test.local             192.168.123.50            14269                has connection
/^[a-zA-Z].+\s{2,}(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	# Use getColData to parse out the data for the specific column from the current line. The current line will be
	# split according to the same separator we've passed in the getColumns function (it's stored in the "columns" variable).
	# If the column cannot be found, the result of getColData is null (not "null").

	row = trim($0)
	connectionState = getColData(row, columns, "Connection state")
	ip = getColData(row, columns, "IP Address")

	if (connectionState == "has connection") {
		serverArr[ip]++
	} else if (ip in serverArr) {
		# Value exists in array but is IP do not have connection this time, do nothing
	} else {
		serverArr[ip] = 0
	}
}

END {
	for (ip in serverArr) {
		if (serverArr[ip] > 1) {
			status = 1
		} else {
			status = 0
		}
		
		tags["name"] = ip
		writeDoubleMetric("identity-integration-connection-state", tags, "gauge", 300, status)
	}
}