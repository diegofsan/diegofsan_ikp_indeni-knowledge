#! META
name: cpstat-mg
description: Shows status of management server
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
    role-management: true
    mds:
        neq: true

#! COMMENTS
mgmt-status:
    why: |
        Unless the management services are running correctly, it might not be possible to manage other gateways.
    how: |
        By using the Check Point built-in "cpstat mg" command, the status of the management server is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Retreiving the management status is only available from the command line.

mgmt-status-description:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 cpstat mg

#! PARSER::AWK

############
# ToDo: Add the status message to a descriptive tag in the future
###########

BEGIN {
	# A lot of data is separated with ":" so using as delimiter instead of whitespace
	FS=":"
}

#Is started:    1
/Is started/ {
	isStarted = trim($2)
}


# Active status: active
# Active status: standby
/Active status/ {
	activeStatusMessage = trim($2)
	if (trim($2) == "active" || trim($2) == "standby") {
		activeStatus = 1
	}
}


# Status:        OK
/Status/ {
	statusMessage = trim($2)
	if (trim($2) == "OK") {
		status = 1
	}
}


END {
	if ( isStarted == 1 && activeStatus == 1 && status == 1 ) {
		mgmtStatus = 1
	} else {
		mgmtStatus = 0
		# Write data collected to troubleshoot
		totalMessages = "is-started: " isStarted " - active-status-message: " activeStatusMessage " - status-message: " statusMessage
		writeComplexMetricString("mgmt-status-description", null, totalMessages)
	}
	# Write metric
	writeDoubleMetricWithLiveConfig("mgmt-status", null, "gauge", "300", mgmtStatus, "Management Services Status", "state", "")
}
