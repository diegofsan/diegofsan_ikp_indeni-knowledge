#! META
name: chkp-os-cphaconf_show_bond
description: run "cphaprob show_bond" to determine bond health
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    high-availability: true
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform

#! COMMENTS
bond-state:
    why: |
        An interface in a bond could be down, and the device would still deem the link as up. This is because the default setting is often to consider one link failure as non-critical if there is another link in the bond. This does however mean a loss of redundancy and capacity which might not be noticed.
    how: |
        Use the built-in "cphaconf show_bond" command to retreive the detailed bond state.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing bond status is only available from the command line interface.

bond-slaves-in-use:
    why: |
        Tracking the bond slaves in use versus the total required is a good indicator of potential issues.
    how: |
        Use the built-in "cphaconf show_bond" command to retreive the detailed bond state.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing bond status is only available from the command line interface.

bond-slaves-required:
    why: |
        Tracking the bond slaves in use versus the total required is a good indicator of potential issues.
    how: |
        Use the built-in "cphaconf show_bond" command to retreive the detailed bond state.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing bond status is only available from the command line interface.

bond-slave-state:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 ifconfig | grep "bond" |awk '{gsub(/\..+/,"",$1); print $1}'| uniq |while read bonds; do ${nice-path} -n 15 cphaconf show_bond $bonds; done

#! PARSER::AWK

############
# Script explanation: The command uses "cphaconf" which is only available when in a cluster.
###########

# Bond name:      bond0
/Bond name/ {
    bondname = $NF
    configured_slave_int=-1
    in_use_slave_interfaces=-1
    required_slave_interfaces=-1
}

# Bond status:    UP
/Bond status/ {
    bondstatus = $NF
    isup = 0
    if (bondstatus == "UP") {
        isup = 1
    }
    bondstatustags["name"] = bondname
    writeDoubleMetricWithLiveConfig("bond-state", bondstatustags, "gauge", "60", isup, "Bond Interfaces", "state", "name")
}

# Configured slave interfaces: 2
/Configured slave interfaces/ {
    configured_slave_int=$NF
}

# In use slave interfaces:     1
/In use slave interfaces/ {
    writeDoubleMetric("bond-slaves-in-use", bondstatustags, "gauge", "60", $NF)
}

# Required slave interfaces:   1
/Required slave interfaces/ {
    writeDoubleMetric("bond-slaves-required", bondstatustags, "gauge", "60", $NF)
}

# Lines should end with status Yes/No, like:
# eth3            | Active          | Yes
/(Yes|No)$/ {
    t["name"] = $1
    state_desc = $NF
#    t["state-description"] = state_desc
    t["bond-name"] = bondname

    up = 0
    if (match(state_desc, "Yes")) {
        up = 1
    }
    writeDoubleMetricWithLiveConfig("bond-slave-state", t, "gauge", "60", up, "Bond Slave Interfaces", "state", "name|bond-name")
}
