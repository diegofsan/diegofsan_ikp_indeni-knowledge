#! META
name: ios-show-processes-memory
description: IOS show process memory
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: ios

#! COMMENTS
process-memory:
    why: |
       Capture the memory utilization for each process running on the system.
       This information is critical for monitoring the system's health and enables debugging of high memory utilization per running process.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show process memory" command. The output includes a table with all running processes and their respective memory utilization.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show processes memory

#! PARSER::AWK

#Processor Pool Total:  396826796 Used:   51942924 Free:  344883872
#      I/O Pool Total:   33554432 Used:    5794240 Free:   27760192
#Transient Pool Total:   16777216 Used:      10152 Free:   16767064
#
# PID TTY  Allocated      Freed    Holding    Getbufs    Retbufs Process
#   0   0   91137744   37903604   50081632        393        112 *Init*          
#   0   0      12136     395412      12136          0          0 *Sched*         
#   0   0   14659264   14388620     204904         26         24 *Dead*          
#   0   0          0          0     394644          0          0 *MallocLite*

BEGIN {
    location = 0
}

/^Processor Pool Total/ {
    # Total memory allocated to processes
    total_mem = trim($4)
}

/Pool Total/ {
    next
}

/(PID|TTY|Allocated)/ {
    next
}

/^\s*[0-9]+\s+[0-9]+.*/ {

    # reset var
    process = ""
    
    pid=trim($1)
    mem_alloc=trim($3)
    mem_hold=trim($5)
    mem_free=trim($4)
    
    for (i=8; i <= NF; i++) {process = process " " trim($i)}
    process = trim(process)
    
    #debugEntries()
    writeMetrics()
}

function debugEntries () {
        writeDebug(pid " id for process " process " is utilizing " 100 * mem_hold / total_mem)
}

function writeMetrics () {
        memtags["name"] = pid
        memtags["process-name"] = process
        mem_percent = 100 * mem_hold / total_mem
        writeDoubleMetric("process-memory", memtags, "gauge", 1, mem_percent)
}

END {
}