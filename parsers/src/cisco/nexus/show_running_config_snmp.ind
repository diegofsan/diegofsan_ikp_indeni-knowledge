#! META
name: nexus-snmp-v2-best-practices
description: Nexus snmp security policies applied
type: monitoring
monitoring_interval: 59 minutes
requires:
    vendor: cisco
    os.name: nxos

# --------------------------------------------------------------------------------------------------
# The script publish the following metrics
#
# [snmp-v2-best-practice]         [true/false, true when snmp is enabled and all communities have acl]
# --------------------------------------------------------------------------------------------------

#! COMMENTS
snmp-v2-best-practice:
    why: |
        Capture whether SNMP v2 is configured according to the best security practises by validating that SNMP community is protected with ACL. SNMP communities are used by SNMP v1/v2c to identify the management system polling SNMP information from the device. Each community can be associated with a different security level (read-only or read/write).
        Note that SNMP communities are transimitted in clear text. More info about CISCO SNMP best practises can be found to the next cisco official link http://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/best_practices/cli_mgmt_guide/cli_mgmt_bp/ip_mgmt.html
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the current state of the SNMP configuration server by using the "show running-config snmp" command.
    without-indeni: |
        The administrator will have to manually log in to the device and check if the SNMPv2 is enabled, the access-level and if it is protected with a community and Access-List
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
show running-config snmp

#! PARSER::AWK

# Initialize the variables
BEGIN {
    # communities variables
    count_snmp_communitiess = 0

    # by default snmp is enabled
    is_snmp_enabled = 1
}

# Fill the 'list_snmp_communities' array, in order to check if all communities have the 'use-acl' property.
# Matching lines:
#snmp-server community helpme! group network-operator
#snmp-server community helpme! use-acl test
/^snmp-server community/ {

    if ($4 == "group") {

        # All snmp communities belong to a group, so we register the community name
        count_snmp_communitiess++
        list_snmp_communities[count_snmp_communitiess, "community"] = $3

    } else if ($4 == "use-acl") {

        # Not all communities have 'use-acl'. We search for the correct community based on name, to set the use-acl property
        indexOf = -1
        for(i = 1; i <= count_snmp_communitiess; i++){
            if (list_snmp_communities[i, "community"] == $3) {
                indexOf = i;
                break;
            }
        }

        if (indexOf !=-1) {
            list_snmp_communities[indexOf, "use-acl"] = $5
        }

    }
}

#### snmp-enabled ####
#SNMP protocol : Disabled
/^no snmp-server protocol enable/ {
    is_snmp_enabled = 0
}


END {

    #
    # Check that all communities have the 'use-acl' property
    #
    is_all_communities_with_acl = 1
    for(i = 1; i <= count_snmp_communitiess; i++) {
            if (length(list_snmp_communities[i, "use-acl"]) == 0) {
                is_all_communities_with_acl = 0
                break
            }
    }

    #
    # Set the is snmp-v2-best-practice variable.
    #
    is_snmp_v2_best_practice = ((is_snmp_enabled == 1 && count_snmp_communitiess > 0 && is_all_communities_with_acl == 1) || (is_snmp_enabled == 0))


    #
    # Publish metrics based on our variables
    #
    snmp_v2_best_practice_value = "false"
    if ( is_snmp_v2_best_practice == 1 ) {
        snmp_v2_best_practice_value = "true"
    }
    writeComplexMetricStringWithLiveConfig("snmp-v2-best-practice", null, snmp_v2_best_practice_value, "SNMP-V2 Best Practice")

    # The [snmp-communities] (list_snmp_communities) is already published from script 'show-snmp.ind' no reasonn to publish it.

}
