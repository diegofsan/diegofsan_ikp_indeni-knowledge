#! META
name: nexus-show-system-resources
description: Nexus show system resources
type: monitoring
monitoring_interval: 2 minutes
includes_resource_data: true
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
cpu-usage:
    why: |
       Capture the utilization of the system's CPU resources.
       This information is critical for monitoring the system's health and make sure the CPU is not over utilized.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show system resources" command. The output includes a table with the device's CPU (per core) utilization statistics and memory information (total, used and free).
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

memory-usage:
    why: |
       Capture the total utilization of the system's memory (RAM) resources.
       This information is critical for monitoring the system's health and make sure memory is not over utilized.
       If memory utilization goes beyond a certain threshold an alert will be triggered.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show show system resources" command. The output includes a table with the device's CPU (per core) utilization statistics and memory information (total, used and free).
    without-indeni: |
       It is possible to poll this data through SNMP in raw format, but the alarm status for memory would have to calculated by analysing the free vs. available values.
    can-with-snmp: false
    can-with-syslog: false

memory-free-kbytes:
    skip-documentation: true

memory-total-kbytes:
    skip-documentation: true

memory-used-kbytes:
    skip-documentation: true

#! REMOTE::SSH
show system resources

#! PARSER::AWK

BEGIN {
}

#     CPU0 states  :   0.0% user,   0.0% kernel,   100.0% idle
/\s+CPU.*states/ {
    cpu_id=$1
    #user=$5
    #kernel=$6
    idle=$8
    #sub(/%/, "", user)
    #sub(/%/, "", kernel)
    sub(/%/, "", idle)

    cputags["cpu-id"] = cpu_id
    cputags["cpu-is-avg"] = "false"
    cputags["resource-metric"] = "true"
    writeDoubleMetricWithLiveConfig("cpu-usage", cputags, "gauge", 60, 100 - idle, "CPU Usage", "percentage", "cpu-id")
}

#Average
#CPU0 states  :   0.0% user,   0.0% kernel,   100.0% idle
/^CPU states/ {
    cpu_id=$1
    #user=$5
    #kernel=$6
    idle=$8
    #sub(/%/, "", user)
    #sub(/%/, "", kernel)
    sub(/%/, "", idle)

    cputags["cpu-id"] = cpu_id
    cputags["cpu-is-avg"] = "true"
    cputags["resource-metric"] = "true"
    writeDoubleMetricWithLiveConfig("cpu-usage", cputags, "gauge", 60, 100 - idle, "CPU Usage", "percentage", "cpu-id")
}

# Memory usage:   8243096K total,   2726536K used,   5516560K free
/^Memory/ {
    total=$3
    used=$5
    free=$7
    sub(/K/, "", total)
    sub(/K/, "", used)
    sub(/K/, "", free)
    memtags["name"] = "System Memory"
    mem_usage = used/total
    writeDoubleMetricWithLiveConfig("memory-free-kbytes", memtags, "gauge", "60", free, "Memory: Free", "kilobytes", "name")
    writeDoubleMetricWithLiveConfig("memory-total-kbytes", memtags, "gauge", "60", total, "Memory: Total", "kilobytes", "name")
    writeDoubleMetricWithLiveConfig("memory-used-kbytes", memtags, "gauge", "60", used, "Memory: Used", "kilobytes", "name")

    memtags["resource-metric"] = "true"    
    writeDoubleMetricWithLiveConfig("memory-usage", memtags, "gauge", "60", mem_usage, "Memory Usage (Percent)", "percentage", "name")
}
