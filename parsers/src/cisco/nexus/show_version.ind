#! META
name: nexus-show-version
description: Nexus show version
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
uptime-seconds:
    why: |
       Capture the uptime of the device. If the uptime is lower than the previous sample, the device must have reloaded. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show version" command. The output includes the device's uptime as well as additional hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP or capture a syslog/trap event of a device booting up.
    can-with-snmp: true
    can-with-syslog: true

vendor:
    why: |
       Capture the device vendor name.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

os-name:
    why: |
       Capture the device operating system name.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

os-version:
    why: |
       Capture the device operating system version. The version should be the same across all members of a vPC (cluster).
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

hostname:
    why: |
       Capture the device hostname.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show version | xml

#! PARSER::AWK

/NX-OS/ {
    ciscoNexus = 1
    writeComplexMetricString("vendor", null, "Cisco")
    writeComplexMetricString("os-name", null, "NX-OS")
}

/Kernel uptime is/ {
    if (!ciscoNexus) {
        next
    }
    split($0, m, " ")
    days = m[4]
    hours = m[6]
    minutes = m[8]
    seconds = m[10]

    timeInSeconds = days*86400 + hours*3600 + minutes*60 + seconds
    writeDebug("uptime " days ":" hours ":" minutes ":" seconds " = " timeInSeconds)
    writeDoubleMetric("uptime-seconds", null, "counter", 300, timeInSeconds)
}

/System version:/ {
    if (!ciscoNexus) {
        next
    }

    split($0, m, ":")
    version = trim(m[2])
    writeComplexMetricString("os-version", null, version)
}

/Device name:/ {
    if (!ciscoNexus) {
        next
    }

    split($0, m, ":")
    hostname = trim(m[2])
    split(hostname, h, "[.]")
    writeComplexMetricString("hostname", null, h[1])
}

