#! META
name: nexus-vpc-role
description: Nexus VPC Role
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    vpc: true

#! COMMENTS
cluster-member-active:
    why: |
       Identify if the device is the active member of a vPC (Virtual Port-Channel) domain (cluster). 
	   vPC domains (clusters) always include 2 Cisco Nexus switches acting as a single logical forwarding switch. Attached devices use a number of links to connect to both switches as if they were a single switch (i.e. the remote devices are configured as if it was a regular single port-channel).
	   For more infomation about Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
	   Certain configuration items have to be in sync across vPC members. If the configuration is out of sync an alert will be generated. This includes things like enabled features and static routes.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC role information using the output of the "show vpc role" command. The output includes the device's vPC state as well as information about the vPC domain and the other vPC peer device.
    without-indeni: |
       It is possible to poll this data through SNMP or capture a syslog/trap event after a switch over has happened.
    can-with-snmp: true
    can-with-syslog: true 

cluster-state:
    why: |
		Identify if there is at least a single active member of a vPC (Virtual Port-Channel) domain (cluster).
		vPC domains (clusters) always include 2 Cisco Nexus switches acting as a single logical forwarding switch. Attached devices use a number of links to connect to both switches as if they were a single switch (i.e. the remote devices are configured as if it was a regular single port-channel).
		For more infomation about Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
		Certain configuration items have to be in sync across vPC members. If the configuration is out of sync an alert will be generated. This includes things like enabled features and static routes.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC role information using the output of the "show vpc role" command. The output includes the device's vPC state as well as information about the vPC domain and the other vPC peer device.
    without-indeni: |
       It is possible to poll this data through SNMP or capture a syslog/trap event after a switch over has happened.
    can-with-snmp: true
    can-with-syslog: true 

#! REMOTE::SSH
show vpc role | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _temp:
            status:
                _text: "${root}/vpc-current-role"
            vpc_system_mac:
                _text: "${root}/vpc-system-mac"
        _tags:
            "im.name":
                _constant: "cluster-member-active"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - This Member State"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                    if (temp("status") == "primary") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
            _tags:
                "name": |
                    {
                        print "vpc:" temp("vpc_system_mac")
                    }
    -
        _temp:
            status:
                _text: "${root}/vpc-peer-status"
            role:
                _text: "${root}/vpc-current-role"
            vpc_system_mac:
                _text: "${root}/vpc-system-mac"
        _tags:
            "im.name":
                _constant: "cluster-state"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Cluster State"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                    if ((temp("role") == "primary") || (temp("status") == "peer-ok")) {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
            _tags:
                "name": |
                    {
                        print "vpc:" temp("vpc_system_mac")
                    }
