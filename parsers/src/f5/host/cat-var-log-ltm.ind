#! META
name: f5-cat-var-log-ltm
description: Check ltm log file for various issues
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
ip-address-conflict:
    why: |
        IP address conflicts can lead to unpredictable behavior in traffic flows. It is important to track these and fix the root cause as soon as possible.
    how: |
        This alert logs into the F5 load balancer and parses the /var/log/ltm file for possible IP address conflicts.
    without-indeni: |
        An administrator would need to log in to the device and look in the var/log/ltm log file for instances of "address conflict detected".
    can-with-snmp: false
    can-with-syslog: true

#! REMOTE::SSH
${nice-path} -n 15 /bin/cat /var/log/ltm | ${nice-path} -n 15 /bin/grep 'address conflict detected for'

#! PARSER::AWK

BEGIN{
    iAddressConflict = 0
}

#Jun  6 14:21:14 F5-LB1 warning tmm[18497]: 01190004:4: address conflict detected for 10.245.0.253 (00:50:56:90:01:22) on vlan 31
/address conflict detected for/ {
    
    logLine = $0
    
    #Remove parts that we are no interested in
    sub(/^.*address conflict detected for /, "", logLine)
    
    #10.245.0.253 (00:50:56:90:01:22) on vlan 31
    split(logLine, arrDescription, /\s/)
    
    ipAddress = arrDescription[1]
    macAddress = arrDescription[2]
    
    #(00:50:56:90:01:22)
    sub(/(\(|\))/, "", macAddress)
    
    #There has been examples on devcentral where vlan id was not there.
    #Might be older versions, or the IP residing on an untagged VLAN, but I'm adding this check here all the same.
    if($(NF-1) == "vlan"){
        vlanId = $NF
    } else {
        vlanId = "n/a"
    }
    
    iAddressConflict++
    addressConflictObj[iAddressConflict, "ip"] = ipAddress
    addressConflictObj[iAddressConflict, "mac-address"] = macAddress
    addressConflictObj[iAddressConflict, "vlan-id"] = vlanId
   
}

END {
    writeComplexMetricObjectArray("ip-address-conflict", null, addressConflictObj)
}