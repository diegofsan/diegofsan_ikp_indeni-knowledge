#! META
name: f5-rest-mgmt-tm-sys-tmm-info
description: Determine tmm memory and cpu usage
type: monitoring
includes_resource_data: true
monitoring_interval: 1 minute
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
memory-usage:
    why: |
        The various memory components of an F5 unit are important to track to ensure a smooth operation. This particlar metric focuses on the memory usage per TMM core.
    how: |
        This script uses the F5 REST API to retrieve the current memory usage of each TMM core.
    without-indeni: |
        These memory metrics are available through SNMP, TMSH and the web user interface under Statistics/Performance.
    can-with-snmp: true
    can-with-syslog: false
cpu-usage:
    why: |
        Tracking the CPU utilization of all processing cores on an F5 unit is critical to ensure capacity isn't reached. Should capacity be reached on data plane cores, new connections and sessions may fail to be processed. This particular alert focuses on the cpu usage per TMM core.
    how: |
        This alert uses the F5 iControl REST API to retrieve the current CPU usage of all TMM cores.
    without-indeni: |
        The utilization of the TMM cores is also available through SNMP and TMSH.
    can-with-snmp: true
    can-with-syslog: false
memory-total-kbytes:
    skip-documentation: true
memory-free-kbytes:
    skip-documentation: true

#! REMOTE::HTTP
url: /mgmt/tm/sys/tmm-info?options=kil&$select=fiveSecAvgUsageRatio,memoryTotal,memoryUsed,tmmId
protocol: HTTPS

#! PARSER::JSON

# This check is a bit of a duplicate as it does a bit of what rest-mgmt-tm-sys-memory, rest-mgmt-tm-sys-cpu and ps aux does.

_metrics:
    -   #Collecting tmm total memory metric
        _groups:
            "$.entries.*.nestedStats.entries[?(@.memoryTotal.value > 0)]":
                _tags:
                    "im.name":
                        _constant: "memory-total-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                    "im.identity-tags":
                        _constant: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Memory - Total"
                _temp:
                    "tmmId":
                        _value: "tmmId.description"
                _value.double:
                    _value: "memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "tmm-core: " temp("tmmId")
                    }
    -   #Collecting tmm free memory metric
        _groups:
            "$.entries.*.nestedStats.entries[?(@.memoryTotal.value > 0)]":
                _tags:
                    "im.name":
                        _constant: "memory-free-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                    "im.identity-tags":
                        _constant: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Memory - Free"

                _temp:
                    "tmmId":
                        _value: "tmmId.description"
                    "memoryUsed":
                        _value: "memoryUsed.value"
                    "memoryTotal":
                        _value: "memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "tmm-core: " temp("tmmId")
                    }
            _value.double: |
                {
                    print temp("memoryTotal") - temp("memoryUsed")
                }
    -   #Collecting tmm memory usage metric
        _groups:
            "$.entries.*.nestedStats.entries[?(@.memoryTotal.value > 0)]":
                _tags:
                    "im.name":
                        _constant: "memory-usage"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "im.identity-tags":
                        _constant: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Memory - Usage"
                    "resource-metric":
                        _constant: "true"
                _temp:
                    "tmmId":
                        _value: "tmmId.description"
                    "memoryUsed":
                        _value: "memoryUsed.value"
                    "memoryTotal":
                        _value: "memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "tmm-core: " temp("tmmId")
                    }
            _value.double: |
                {
                    memoryTotal = temp("memoryTotal")
                    memoryUsed = temp("memoryUsed")
                    
                    print (memoryUsed/memoryTotal)*100
                    
                }
    -   #Collecting used tmm cpu usage metric
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "cpu-usage"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "cpu-is-avg":
                        _constant: "false"
                    "im.identity-tags":
                        _constant: "cpu-id"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "CPU"
                    "resource-metric":
                        _constant: "true"
                _temp:
                    "tmmId":
                        _value: "tmmId.description"
                _value.double:
                    _value: "fiveSecAvgUsageRatio.value"

        _transform:
            _tags:
                "cpu-id": |
                    {
                        print "tmm-core: " temp("tmmId")
                    }
