#! META
name: junos-show-configuration-domain-name
description: JUNOS show the domain-name in the configuration
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
domain:
    why: |
       Capture the domain of the device. This is used for inventory purposes.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the relevant output of the "show configuration system domain-name" command.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show configuration system domain-name 

#! PARSER::AWK
#domain-name gongya.net;
/^(domain-name \S+;)/ {
    domainName = $2
    sub(/;/, "", domainName)
    writeComplexMetricStringWithLiveConfig("domain", null, domainName, " Overview")
}
