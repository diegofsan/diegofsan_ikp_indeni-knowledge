#! META
name: junos-show-version
description: JUNOS show version
type: monitoring
monitoring_interval: 1440 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
model:
    why: |
       Capture the device model.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false
vendor:
    why: |
       Capture the device vendor name.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false
hostname:
    why: |
       Capture the host name of the device. This is used for inventory purposes.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false
os-name:
    why: |
       Capture the device operating system name.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false
os-version:
    why: |
       Capture the device operating system version. The version should be the same across all members of a cluster.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show version" command. The output includes the device's hardware and software related details.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false
software-eos-date:
    why: |
        Ensuring the software being used is always within the vendor's list of supported versions is critical. Otherwise, during a critical issue, the vendor may decline to provide technical support. Juniper posts the list of supported software on their website ( http://www.juniper.net/support/eol/junos.html ). indeni tracks that list and updates this script to match.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH to retrieve the current software version and based on the software version and the Juniper provided information at http://www.juniper.net/support/eol/junos.html the correct end of support date is used.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when a given device may be nearing its software end of support and is in need of upgrading.
    can-with-snmp: false
    can-with-syslog: false
hardware-eos-date:
    why: |
        Ensuring the hardware being used is always within the vendor's list of supported models is critical. Otherwise, during a critical issue, the vendor may decline to provide technical support. Juniper posts the list of supported hardware on their website ( http://www.juniper.net/support/eol/srxseries_hw.html ). indeni tracks that list and updates this script to match.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH to retrieve the current model used and based on it and the Juniper provided information at http://www.juniper.net/support/eol/srxseries_hw.html the correct end of support date is used.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when a given device may be nearing its end of support and is in need of replacement.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show version | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//software-information[1]
_metrics:
    -
        _tags:
            "im.name":
                _constant: "vendor"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: " Overview"
        _value.complex:
            value:
                _constant: "Juniper"
    -
        _tags:
            "im.name":
                _constant: "os-name"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: " Overview"
        _value.complex:
            value:
                _constant: "JUNOS"
    -
        _tags:
            "im.name":
                _constant: "os-version"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: " Overview"
        _temp:
            version:
                _text: "${root}/package-information[name = 'junos']/comment"
        _transform:
            _value.complex:
                value: |
                    {
                        comment = temp("version")
                        sub(/^[^\[]+\[/, "", comment)
                        sub(/]/, "", comment)
                        print comment
                    }
    -
        _tags:
            "im.name":
                _constant: "model"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: " Overview"
        _value.complex:
            value: 
                _text: "${root}/product-model"
    -
        _tags:
            "im.name":
                _constant: "hostname"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: " Overview"
        _value.complex:
            value: 
                _text: "${root}/host-name"
    -
        _tags:
            "im.name":
                _constant: "hardware-eos-date"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "End of Support - Hardware"
            "im.dstype.displayType":
                _constant: "date"  
        _temp:
            model:
                _text: /rpc-reply/software-information[contains(' srx110h2 srx110h2-vb srx100 srx210 srx240 srx650 srx110h srx110h-taa srx210he-taa srx210he-poe-taa srx240h-taa srx240h-poe-taa srx240h srx100b srx100h srx110h-va srx100b srx210he srx110h-vb srx210be srx210he-poe srx220h srx220h-poe srx240b srx240b2 srx240h srx240h-poe srx240h-dc srx210b srx210h srx210h-poe srx210h-p-mgw srx220h-p-mgw srx240h-p-mgw ', concat(' ', product-model, ' '))]/product-model 
        _transform:
            _value.double: |
                {
                    eosdates["srx110h2"] = date(2022, 03, 31)
                    eosdates["srx110h2-vb"] = date(2022, 03, 31)
                    eosdates["srx100"] = date(2021, 05, 01)
                    eosdates["srx210"] = date(2021, 05, 01)
                    eosdates["srx240"] = date(2021, 05, 01)
                    eosdates["srx650"] = date(2021, 05, 01)
                    eosdates["srx110h"] = date(2020, 11, 30)
                    eosdates["srx110h-taa"] = date(2020, 11, 30)
                    eosdates["srx210he"] = date(2020, 11, 30)
                    eosdates["srx210he-taa"] = date(2020, 11, 30)
                    eosdates["srx210he-poe-taa"] = date(2020, 11, 30)
                    eosdates["srx240h-taa"] = date(2020, 11, 30)
                    eosdates["srx240h-poe-taa"] = date(2020, 11, 30)
                    eosdates["srx240h"] = date(2020, 11, 30)
                    eosdates["srx100b"] = date(2019, 05, 10)
                    eosdates["srx100h"] = date(2019, 05, 10)
                    eosdates["srx110h-va"] = date(2019, 05, 10)
                    eosdates["srx110h-vb"] = date(2019, 05, 10)
                    eosdates["srx210be"] = date(2019, 05, 10)
                    eosdates["srx210he"] = date(2019, 05, 10)
                    eosdates["srx210he-poe"] = date(2019, 05, 10)
                    eosdates["srx220h"] = date(2019, 05, 10)
                    eosdates["srx220h-poe"] = date(2019, 05, 10)
                    eosdates["srx240b"] = date(2019, 05, 10)
                    eosdates["srx240b2"] = date(2019, 05, 10)
                    eosdates["srx240h"] = date(2019, 05, 10)
                    eosdates["srx240h-poe"] = date(2019, 05, 10)
                    eosdates["srx240h-dc"] = date(2019, 05, 10)
                    eosdates["srx210b"] = date(2017, 08, 31)
                    eosdates["srx210h"] = date(2017, 08, 31)
                    eosdates["srx210h-poe"] = date(2017, 08, 31)
                    eosdates["srx210h-p-mgw"] = date(2011, 01, 24)
                    eosdates["srx220h-p-mgw"] = date(2011, 01, 24)
                    eosdates["srx240h-p-mgw"] = date(2011, 01, 24)

                    print eosdates[temp("model")]
                }
    -
        _tags:
            "im.name":
                _constant: "software-eos-date"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "End of Support - Software"
            "im.dstype.displayType":
                _constant: "date"  
        _temp:
            version:
                _text:  /rpc-reply/software-information/package-information/comment[contains(text(),'[16.2') or 
                                                                                    contains(text(),'[16.1') or
                                                                                    contains(text(),'[15.1X49') or
                                                                                    contains(text(),'[15.1') or
                                                                                    contains(text(),'[14.2') or
                                                                                    contains(text(),'[14.1X5') or
                                                                                    contains(text(),'[14.1') or
                                                                                    contains(text(),'[13.3') or
                                                                                    contains(text(),'[13.2X5') or
                                                                                    contains(text(),'[13.2') or
                                                                                    contains(text(),'[13.1X5') or
                                                                                    contains(text(),'[13.1') or
                                                                                    contains(text(),'[12.3X54') or
                                                                                    contains(text(),'[12.3X52') or
                                                                                    contains(text(),'[12.3X51') or
                                                                                    contains(text(),'[12.3X50') or
                                                                                    contains(text(),'[12.3X48') or
                                                                                    contains(text(),'[12.31') or
                                                                                    contains(text(),'[12.2X5') or
                                                                                    contains(text(),'[12.2') or
                                                                                    contains(text(),'[12.1X4') or
                                                                                    contains(text(),'[12.1X4') or
                                                                                    contains(text(),'[12.1X47') or
                                                                                    contains(text(),'[12.1X46') or
                                                                                    contains(text(),'[12.1X45') or
                                                                                    contains(text(),'[12.1X44') or
                                                                                    contains(text(),'[12.1') or
                                                                                    contains(text(),'[11.4') or
                                                                                    contains(text(),'[11.3') or
                                                                                    contains(text(),'[11.2') or
                                                                                    contains(text(),'[11.1') or
                                                                                    contains(text(),'[10.4') or
                                                                                    contains(text(),'[10.3') or
                                                                                    contains(text(),'[10.2') or
                                                                                    contains(text(),'[10.1') or
                                                                                    contains(text(),'[10.0') or
                                                                                    contains(text(),'[9.6') or
                                                                                    contains(text(),'[9.5') or
                                                                                    contains(text(),'[9.4') or
                                                                                    contains(text(),'[9.3') or
                                                                                    contains(text(),'[9.2') or
                                                                                    contains(text(),'[9.1') or
                                                                                    contains(text(),'[9.0') or
                                                                                    contains(text(),'[8.5') or
                                                                                    contains(text(),'[8.4') or
                                                                                    contains(text(),'[8.3') or
                                                                                    contains(text(),'[8.2') or
                                                                                    contains(text(),'[8.1') or
                                                                                    contains(text(),'[8.0') or
                                                                                    contains(text(),'[7.6') or
                                                                                    contains(text(),'[7.5') or
                                                                                    contains(text(),'[7.4') or
                                                                                    contains(text(),'[7.3') or
                                                                                    contains(text(),'[7.2') or
                                                                                    contains(text(),'[7.1') or
                                                                                    contains(text(),'[7.0') or
                                                                                    contains(text(),'[6.4') or
                                                                                    contains(text(),'[6.3') or
                                                                                    contains(text(),'[6.2') or
                                                                                    contains(text(),'[6.1') or
                                                                                    contains(text(),'[6.0') or
                                                                                    contains(text(),'[5.7') or
                                                                                    contains(text(),'[5.6') or
                                                                                    contains(text(),'[5.5') or
                                                                                    contains(text(),'[5.4') or
                                                                                    contains(text(),'[5.3') or
                                                                                    contains(text(),'[5.2') or
                                                                                    contains(text(),'[5.1') or
                                                                                    contains(text(),'[5.0') or
                                                                                    contains(text(),'[4.4') or
                                                                                    contains(text(),'[4.3') or
                                                                                    contains(text(),'[4.2') or
                                                                                    contains(text(),'[4.1') or
                                                                                    contains(text(),'[4.0') 
                                                                                   ]
        _transform:
            _value.double: |
                {
                    version = temp("version")
                    sub(/^[^\[]+\[/, "", version)
                    sub(/]/, "", version)

                    if (version ~ /^16.2/) { print date(2020, 05, 29) }
                    else if (version ~ /^16.1/) { print date(2020, 01, 28) }
                    else if (version ~ /^15.1X49/) { print date(2020, 05, 01) }
                    else if (version ~ /^15.1/) { print date(2018, 12, 05) }
                    else if (version ~ /^14.2/) { print date(2018, 05, 05) }
                    else if (version ~ /^14.1X5/) { print date(2019, 06, 30) }
                    else if (version ~ /^14.1/) { print date(2018, 06, 13) }
                    else if (version ~ /^13.3/) { print date(2017, 07, 22) }
                    else if (version ~ /^13.2X5/) { print date(2017, 06, 30) }
                    else if (version ~ /^13.2/) { print date(2016, 02, 29) }
                    else if (version ~ /^13.1X5/) { print date(2015, 12, 30) }
                    else if (version ~ /^13.1/) { print date(2015, 09, 15) }
                    else if (version ~ /^12.3X54/) { print date(2018, 07, 18) }
                    else if (version ~ /^12.3X52/) { print date(2016, 02, 23) }
                    else if (version ~ /^12.3X51/) { print date(2015, 09, 15) }
                    else if (version ~ /^12.3X50/) { print date(2016, 07, 31) }
                    else if (version ~ /^12.3X48/) { print date(2022, 06, 30) }
                    else if (version ~ /^12.31/) { print date(2016, 07, 31) }
                    else if (version ~ /^12.2X5/) { print date(2015, 07, 31) }
                    else if (version ~ /^12.2/) { print date(2015, 03, 05) }
                    else if (version ~ /^12.1X4/) { print date(2014, 10, 19) }
                    else if (version ~ /^12.1X4/) { print date(2015, 06, 30) }
                    else if (version ~ /^12.1X47/) { print date(2017, 02, 18) }
                    else if (version ~ /^12.1X46/) { print date(2017, 06, 30) }
                    else if (version ~ /^12.1X45/) { print date(2015, 01, 17) }
                    else if (version ~ /^12.1X44/) { print date(2016, 07, 18) }
                    else if (version ~ /^12.1/) { print date(2014, 09, 28) }
                    else if (version ~ /^11.4/) { print date(2015, 06, 21) }
                    else if (version ~ /^11.3/) { print date(2013, 03, 15) }
                    else if (version ~ /^11.2/) { print date(2013, 02, 15) }
                    else if (version ~ /^11.1/) { print date(2012, 05, 15) }
                    else if (version ~ /^10.4/) { print date(2014, 06, 08) }
                    else if (version ~ /^10.3/) { print date(2011, 12, 21) }
                    else if (version ~ /^10.2/) { print date(2011, 11, 15) }
                    else if (version ~ /^10.1/) { print date(2011, 05, 15) }
                    else if (version ~ /^10.0/) { print date(2013, 05, 15) }
                    else if (version ~ /^9.6/) { print date(2010, 11, 06) }
                    else if (version ~ /^9.5/) { print date(2010, 08, 15) }
                    else if (version ~ /^9.4/) { print date(2010, 05, 11) }
                    else if (version ~ /^9.3/) { print date(2012, 05, 15) }
                    else if (version ~ /^9.2/) { print date(2009, 11, 12) }
                    else if (version ~ /^9.1/) { print date(2009, 07, 28) }
                    else if (version ~ /^9.0/) { print date(2009, 05, 15) }
                    else if (version ~ /^8.5/) { print date(2011, 05, 16) }
                    else if (version ~ /^8.4/) { print date(2008, 11, 09) }
                    else if (version ~ /^8.3/) { print date(2008, 07, 18) }
                    else if (version ~ /^8.2/) { print date(2008, 05, 15) }
                    else if (version ~ /^8.1/) { print date(2010, 05, 06) }
                    else if (version ~ /^8.0/) { print date(2007, 11, 15) }
                    else if (version ~ /^7.6/) { print date(2007, 08, 15) }
                    else if (version ~ /^7.5/) { print date(2007, 05, 08) }
                    else if (version ~ /^7.4/) { print date(2007, 02, 15) }
                    else if (version ~ /^7.3/) { print date(2006, 11, 16) }
                    else if (version ~ /^7.2/) { print date(2006, 08, 14) }
                    else if (version ~ /^7.1/) { print date(2006, 05, 14) }
                    else if (version ~ /^7.0/) { print date(2006, 02, 15) }
                    else if (version ~ /^6.4/) { print date(2005, 11, 12) }
                    else if (version ~ /^6.3/) { print date(2005, 08, 15) }
                    else if (version ~ /^6.2/) { print date(2005, 05, 15) }
                    else if (version ~ /^6.1/) { print date(2005, 02, 15) }
                    else if (version ~ /^6.0/) { print date(2004, 11, 15) }
                    else if (version ~ /^5.7/) { print date(2004, 08, 15) }
                    else if (version ~ /^5.6/) { print date(2004, 05, 15) }
                    else if (version ~ /^5.5/) { print date(2004, 02, 15) }
                    else if (version ~ /^5.4/) { print date(2003, 11, 15) }
                    else if (version ~ /^5.3/) { print date(2003, 08, 15) }
                    else if (version ~ /^5.2/) { print date(2003, 05, 15) }
                    else if (version ~ /^5.1/) { print date(2003, 02, 15) }
                    else if (version ~ /^5.0/) { print date(2002, 11, 15) }
                    else if (version ~ /^4.4/) { print date(2002, 08, 15) }
                    else if (version ~ /^4.3/) { print date(2002, 05, 15) }
                    else if (version ~ /^4.2/) { print date(2002, 02, 15) }
                    else if (version ~ /^4.1/) { print date(2001, 11, 15) }
                    else if (version ~ /^4.0/) { print date(2001, 08, 15) }
                    #else { print null }
                }
