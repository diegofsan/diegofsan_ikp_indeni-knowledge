=======
##Version 6.0.2

*Cross vendor*
IKP-1057	"License expired" add option to ignore licenses expiration more than x months ago 
IKP-1109	Rules: Duplicate Rules Removal
IKP-737	RULE: missing rule to detect when snmp is public

*Check Point*
IKP-1087	"Failed to communicate" - secondary MDS and MLM
IKP-1105	IPSO - Cluster Down, affected: VRRP
IKP-1107	name collision: "detect-management-interrogation"
IKP-738	Alert when there are repeated unsuccessful logon attempts
IKP-751	Correct #!COMMENTS for Description in vpn-check-tunnels-novsx.ind and clean up
IKP-795	SPLAT load-average ind script syntax improvement in prep for regression testing
IKP-835	Resolved issue with parser returning input failure  - chkp-os-failed-login
IKP-848	Updated IND script with further documentation to include  #!COMMENTS section
IKP-849	snmp-enabled written twice per run of ind script
IKP-856	Resolved issue. Alert will now trigger when metric log-file-size increases
IKP-876	Resolved issue. Throughput alert will trigger based on VSID
IKP-916	Resolved issue. Alert will properly trigger when Core dump files found
IKP-927	IPSO - interface and cluster state logic improvements
IKP-943	Improve logic in IND script to rewrite ntpq-p.ind to not use ntp-servers
IKP-983	Live config for cluster information corrected parameter
IKP-984	Resolved issue. If a cluster member is passing traffic, the "cluster" is not down, it is still up. No alert will be issued

*Cisco Nexus*
IKP-1009	Improve remediation steps for select Cisco Nexus rules
IKP-1043	Cisco NXOS Renaming back files and rule names
IKP-1045	add back the indeni legacy
IKP-1114	Update template rule "NextHopRouterInaccessibleRule", adding remediation text for nexus
IKP-1130	Remove duplicate command names in IND scripts
IKP-350	Create a new alert monitoring changes of "spanning-tree-root" metrics
IKP-463	Ensure SNMP communities are enabled with an ACL
IKP-788	Add output and input files for Cisco IOS and NXOS
IKP-826	Improved logic. Modify snmp-enabled from enabled/disabled to true/false
IKP-855	Ensure that SNMPv3 is configured, validate that SNMP requires authentication or encryption for all incoming requests
IKP-938	Monitor the results from the diagnostic tests for the Nexus Fabric Extender chassis
IKP-972	Unexpected too high number of commands are executed from the Indeni Server 

*F5*
IKP-290	Alert created for "Reset cause packet details enabled"

*Fortinet FortiGate*
IKP-1119	fortinet/fortigate/get_system_status... should use dashes, not underscores in the file name
IKP-1123	review "memory-usage" #! COMMENTS in monitoring script
IKP-866	Interrogation script for fortinet/fortigate

*Palo Alto Networks*
IKP-1219	Alert on environmental temperature value based on threshold 
IKP-1224	Add new metric "missing-power-supply", introduce "hardware-element-state" for PAN power supplies, and new alert "Power Supply Missing"

*Radware Alteon*
IKP-696	Radware: add ntp-server-state metric
IKP-861	Radware: add ntp-servers metric
IKP-867	radware: add lb-snatpool-usage and lb-snatpool-limit metric
IKP-869	Radware: stats-ntp.ind broken
IKP-870	Radware Alteon - change monitoring interval to < 60 minutes

*Misc/Internal*
IKP-1156	Fix command-runner-aliases to look in the ./test directory instead of ./input
IKP-1196	add vendor string for bluecoat and fortinet
IKP-1237	Fixing regexp in tmsh-show-vcmp-health.ind


##Version 6.0.1 - Hot fix
* IKP-1216 : For each file in this change, adds a non-existing "requires:" tag with a dummy value. This is to "disable" these scripts. The idea is that, because the tag does not exist, the script will never run. This does not appear to add any ERRORs to the collector log. See ticket for more details. To revert, just remove the added line.
* IKP-1107 : name collision: "detect-management-interrogation"

##Version 6.0.0
###Feature - IS-2723:: Improve AWK parser 
The AWK parser consumes *a lot* of CPU time -- way more than it should. We should improve its performance. 
According to the documentation in the [JAWK website](http://jawk.sourceforge.net/), there are two potentials ways in which we could improve the parser's performance significantly:
* Compile the AWK scripts on start-up instead of using an interpreter each time.
* Add a custom extension for the built-in AWK functions (in {{indeni.hawk}}) that'll remove our need to parse the AWK script's output with regex-es (which is also a very heavy operation).
