package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}


case class ClusterPreemptionEnabledRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_cluster_preempt", "Clustered Devices: Cluster has preemption enabled",
    "Preemption is generally a bad idea in clustering, although sometimes it is the default setting. Indeni will alert if it's on.",
    AlertSeverity.ERROR).interval(TimeSpan.fromMinutes(5)).build()


  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("cluster-preemption-enabled").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-preemption-enabled"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        Equals(
          inUseValue,
          ConstantExpression[Option[Double]](Some(1.0)))
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("This cluster member has preemption enabled. This means that it will have priority over other cluster members. If this device reboots or crashes, it'll try to assume priority in the cluster when it finishes its boot process. This may result in it crashing again, and causing a preemption loop."),
      ConditionalRemediationSteps("It is generally best to have preemption disabled. Instead, once this device returns from a crash, you can conduct the failover manually.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Palo Alto Networks firewalls have a special way of handling preemption loops, see https://live.paloaltonetworks.com/t5/Learning-Articles/Understanding-Preemption-with-the-Configured-Device-Priority-in/ta-p/53398",
        ConditionalRemediationSteps.OS_NXOS ->
          """FHRP preemption and delays features are not required. The vPC will forward traffic as soon as the links become available. Once a device recovers from a crash or reboot, you can conduct the failover manually.
            |Cisco recommends:
            |1. Configuring the FHRP with the default settings and without preempt when using vPC.
            |2. Make the vPC primary switch the FHRP active switch. This is not intended to improve performance or stability. It does make one switch responsible for the control plane traffic. This is a little easier on the administrator while troubleshooting.""".stripMargin
      )
    )
  }
}
