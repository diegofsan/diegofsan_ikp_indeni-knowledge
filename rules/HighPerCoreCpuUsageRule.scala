package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.MinExpression
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class HighPerCoreCpuUsageRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_CPU_Usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of CPU Usage",
    "What is the threshold for the CPU usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    70.0)

  private[library] val minimumNumberOfCoresParameterName = "higher_than_threshold_cores"
  private val minimumNumberOfCoresParameter = new ParameterDefinition(minimumNumberOfCoresParameterName,
    "",
    "Number of Cores",
    "The number of CPU cores with usage above the value set in " +
      "\"" + highThresholdParameterName + "\"" +
      " before an alert is issued.",
    UIType.INTEGER,
    1)

  private[library] val timeThresholdParameterName = "time_threshold"
  private val timeThresholdParameter = new ParameterDefinition(timeThresholdParameterName,
    "",
    "Time Threshold",
    "The CPU cores need to remain above the value set in " +
      "\"" + highThresholdParameterName + "\"" +
      " for this amount of time before an alert is issued.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(10))


  override def metadata: RuleMetadata = RuleMetadata.builder("high_per_core_cpu_use_by_device", "All Devices: High CPU usage per core(s)",
    "High CPU usage is a symptom of a system which is unable to handle " +
    "the required load or a symptom of a specific issue with the system " +
    "and the applications and services running on it. indeni will monitor the CPU usage " +
      "of each core separately and alert if any of the cores' CPU usage crosses the threshold.",
    AlertSeverity.ERROR).configParameters(highThresholdParameter, minimumNumberOfCoresParameter, timeThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = MinExpression(TimeSeriesExpression[Double]("cpu-usage"))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("cpu-id", "cpu-is-avg"), True),

        StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("cpu-usage"), historyLength = getParameterTimeSpanForRule(timeThresholdParameter)),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              And(
                ScopeValueExpression("cpu-is-avg").visible().isIn(Set("true")).not,
                GreaterThanOrEqual(
                  inUseValue,
                  getParameterDouble(highThresholdParameter)))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"cpu-id\")}"),
                scopableStringFormatExpression("Current CPU utilization is: %.0f%%", inUseValue),
                title = "Cores with High CPU Usage"
            ).asCondition(minimumIssueCount = getParameterInt(minimumNumberOfCoresParameter).expr)
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some CPU cores are under high usage."),
        ConditionalRemediationSteps("Determine the cause for the high CPU usage of the listed cores.",
          ConditionalRemediationSteps.VENDOR_CP -> "An extremely helpful article on high CPU utilization on Check Point firewalls is available here: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98348",
          ConditionalRemediationSteps.VENDOR_PANOS -> "For MP (management plane) CPU, look at https://live.paloaltonetworks.com/t5/Featured-Articles/Tips-amp-Tricks-Reducing-Management-Plane-Load/ta-p/64681 . For DP (data plane), read https://live.paloaltonetworks.com/t5/Featured-Articles/How-to-Troubleshoot-High-Dataplane-CPU/ta-p/73000"
        )
    )
  }
}

object HighPerCoreCpuUsageRule {

  /* --- Constants --- */

  private[library] val NAME = "high_per_core_cpu_use_by_device"
}
