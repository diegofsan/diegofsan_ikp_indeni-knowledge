package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NextHopRouterInaccessibleRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_next_hop_router_inaccessible", "All Devices: Next hop inaccessible",
    "indeni will review the routing table and identify when a next hop router is showing as FAILED or INCOMPLETE in the ARP table.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectSnapshotsExpression(context.snapshotsDao, Set("arp-table", "static-routing-table")).multi(),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          StatusTreeExpression(
            IterateSnapshotDimensionExpression("arp-table", "static-routing-table"),
              And(
                Equals[String, String](
                  ScopeValueExpression("targetip").invisible("arp-table").optional(),
                  ScopeValueExpression("next-hop").invisible("static-routing-table").optional()
                ),
                Equals(
                  ScopeValueExpression("success").invisible("arp-table").optional(),
                  ConstantExpression(Some("0"))
                )
              )
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"static-routing-table:next-hop\")}"),
              EMPTY_STRING,
              title = "Inaccessible Next Hops"
          ).asCondition()
        ).withoutInfo().asCondition()


      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("Some of the routes in this device have a next hop which is inaccessible."),
        ConditionalRemediationSteps("Determine why the next hops are not responding.",
          ConditionalRemediationSteps.VENDOR_CP -> "Trying pinging the next hop routers in the list above and resolve any connectivity issues one by one until all pings are successful.",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device over SSH and review the output of \"show arp\" to identify failures.",
                    ConditionalRemediationSteps.OS_NXOS ->
            """|
              |1. Execute the "show spanning-tree" and "show spanning-tree summary"  NX-OS commands to quickly identify the STP root for all the configured vlans.
              |2. Run the "show spanning-tree vlan X detail" NX-OS command to collect more info about the STP topology (X=vlanid).
              |3. Check the event history to find where the Topology Change Notifications originate from by running the next NX-OS command "show spanning-tree internal event-history tree X brief" , (X=vlanid).
              |4. Display the STP events of an interface with the next NX-OS command "show spanning-tree internal event-history tree Y interface X brief" , (X=vlanid, Y=interfaceid).
              |5. Consider to hard code the STP root and backup root to the core switches by configuring a lower STP priority.
              |6. Activate the recommended vPC "peer switch" NX-OS command to a pure peer switch topology in which the devices all belong to the vPC.
              |7. Consider to use Root Guard feature to enforce the root bridge placement in the network. If a received BPDU triggers an STP convergence that makes that designated port become a root port, that port is put into a root-inconsistent (blocked) state.
              |8. For more information please review the following links:
              | <a target="_blank" href="https://www.cisco.com/c/en/us/support/docs/switches/nexus-5000-series-switches/116199-technote-stp-00.html">Spanning Tree Protocol Troubleshooting on a Nexus 5000 Series Switch</a>
              | <a target="_blank" href="https://www.cisco.com/c/dam/en/us/products/collateral/switches/nexus-7000-series-switches/C07-572834-00_STDG_NX-OS_vPC_DG.pdf">Spanning Tree Design Guidelines for Cisco NX-OS Software and Virtual PortChannels</a>
            """.stripMargin
        )
    )
  }
}
