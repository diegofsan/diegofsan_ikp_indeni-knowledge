package com.indeni.server.rules.library.paloalto

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LogsDiscardedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("panw_logs_discarded", "Palo Alto Networks Firewalls: Logs are being discarded",
    "indeni will alert if logs, or forwarded logs, are being discarded.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue1 = TimeSeriesExpression[Double]("logs-discarded-queue-full").last
    val actualValue2 = TimeSeriesExpression[Double]("log-forward-discarded-queue-full").last
    val actualValue3 = TimeSeriesExpression[Double]("log-forward-discarded-send-error").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("logs-discarded-queue-full", "log-forward-discarded-queue-full", "log-forward-discarded-send-error"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            com.indeni.ruleengine.expressions.conditions.Or(
              GreaterThan(actualValue1, ConstantExpression(Some(0.0))),
              GreaterThan(actualValue2, ConstantExpression(Some(0.0))),
              GreaterThan(actualValue3, ConstantExpression(Some(0.0)))
            )
          ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Certain logs are being discarded. This alert is based on tracking the \"discarded\" counters in \"debug log-receiver statistics\"."),
        ConstantExpression("Determine why logs are being discarded - are there too many logs being generated, or is it a connectivity issue?")
    )
  }
}
