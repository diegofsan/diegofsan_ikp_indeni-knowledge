package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class chkp_cluster_id_conflict(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "chkp_cluster_id_conflict",
  ruleFriendlyName = "Check Point ClusterXL: Cluster ID settings conflict",
  ruleDescription = "There are two ways to configure the cluster ID in a Check Point cluster - through cphaconf and through the kernel parameters. If both are used, odd issues may occur.",
  metricName = "chkp-cluster-id-conflict",
  alertDescription = "The device is mixing two methods for configuring the cluster ID or magic MAC. This is not supported and can cause undesirable consequences.",
  baseRemediationText = "Follow sk25977 on how to configure the cluster ID, depending on the version you are using.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("chkp-cluster-id-conflict").asSingle().mostRecent().noneable))()
