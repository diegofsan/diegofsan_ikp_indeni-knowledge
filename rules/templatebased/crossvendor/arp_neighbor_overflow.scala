package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityTemplateRule}

/**
  *
  */
case class arp_neighbor_overflow(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "arp_neighbor_overflow",
  ruleFriendlyName = "All Devices: High ARP cache usage",
  ruleDescription = "Indeni will alert when the number of ARP entries stored by a device is nearing the allowed limit.",
  usageMetricName = "arp-total-entries",
  limitMetricName = "arp-limit",
  threshold = 80.0,
  alertDescriptionFormat = "The ARP table has %.0f entries where the limit is %.0f.\n\nThis alert was added per the request of Mart Khizner (Leumi Card).",
  baseRemediationText = "Identify the cause of the large ARP table. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review sk43772: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk43772",
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show iparp" NX-OS  command to display the Address Resolution Protocol (ARP) table statistics. Note: You must use the feature interface-vlan command before you can display the ARP information for VLAN interfaces.
      |2. Review the ARP table for unknown hosts which may saturate the ARP table of the switch.
      |3. If the number of ARP entries is normal then consider to upgrade the Nexus switch since it is close to the ARP limit capacity.
      |4. For more information review the next Cisco Configuration  guide: https://www.cisco.com/c/m/en_us/techdoc/dc/reference/cli/n5k/commands/show-ip-arp.html""".stripMargin
)
