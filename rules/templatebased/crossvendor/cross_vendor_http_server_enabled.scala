package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_http_server_enabled(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_http_server_enabled",
  ruleFriendlyName = "All Devices: An HTTP server is enabled on the device",
  ruleDescription = "Indeni will check if a device has the HTTP service enabled. HTTP is not encrypted and is therefore a security risk.",
  metricName = "http-server-enabled",
  alertDescription = "The HTTP server allows unencrypted control traffic to network devices. It transmits all data in clear text, including passwords and other potentially confidential information.",
  baseRemediationText = "Disable the HTTP server on the device.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("http-server-enabled").asSingle().mostRecent().noneable))(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Disable the HTTP server on the device. You can do so by using the "no feature http-server" configuration command.
      |2. You can verify that HTTP has been disabled by using the "show http-server" command.""".stripMargin
)
