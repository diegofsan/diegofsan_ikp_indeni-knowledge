package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_mgmt_component_down_novsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_mgmt_component_down_novsx",
  ruleFriendlyName = "Management Devices: Management service down (Non-Virtual)",
  ruleDescription = "Alert if the management component is down on a device.",
  metricName = "mgmt-status",
  stateDescriptionComplexMetricName = "mgmt-status-description",
  alertDescription = "The management component on this device is down.",
  metaCondition = !DataEquals("vsx", "true"),
  baseRemediationText = "This may be due to someone stopping the management component itself, a licensing or a performance issue.")(
  ConditionalRemediationSteps.VENDOR_CP -> "The management service is handled by the \"fwm\" process. Run \"cpstat mg\" for more details. Review the licenses installed on the device, as well as whether or not anyone has run cpstop recently."
)
