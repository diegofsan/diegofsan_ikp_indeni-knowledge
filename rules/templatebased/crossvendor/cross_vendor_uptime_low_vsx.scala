package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NumericThresholdOnDoubleMetricWithItemsTemplateRule, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

/**
  *
  */
case class cross_vendor_uptime_low_vsx(context: RuleContext) extends NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
  ruleName = "cross_vendor_uptime_low_vsx",
  ruleFriendlyName = "All Devices (VSX): Virtual systems restarted (uptime low)",
  ruleDescription = "Indeni will alert when a virtual system has restarted.",
  severity = AlertSeverity.CRITICAL,
  metricName = "uptime-seconds",
  threshold = TimeSpan.fromMinutes(60),
  thresholdDirection = ThresholdDirection.BELOW,
  applicableMetricTag = "vs.name",
  alertItemsHeader = "Affected Virtual Systems",
  alertItemDescriptionFormat = "The current uptime is %.0f seconds which seems to indicate the virtual system has restarted.",
  alertDescription = "Some virtual systems on this device have restarted recently. Review the list below.",
  baseRemediationText = "Determine why the virtual system(s) was restarted.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show version" or "show system reset-reason" NX-OS commands to display the reason for the reload
      |2. Use the "show cores" command to determine if a core file was recorded during the unexpected reboot
      |3.  Run the "show process log" command to display the processes and if a core was created.
      |4.  With the show logging command, review the events that happened close to the time of reboot
    """.stripMargin
)
