#####################################################################################
#
#                                   Helping aliases
#
#####################################################################################

#Credentials
USER="indeni"
PASSWORD="indenirocks123!"

#Default devices unless an IP is specified
DEFAULTF5="192.168.197.50"

#####################################################################################
#                                   Command-Runner full-command
#
#   Description:
#   Runs a script, or a directory of indeni scripts in full-command mode
#
#   Usage:
#   crfull <script name or directory name> <device ip> [verbose]
#
#   Examples:
#   Run a command against 192.168.197.51
#   crfull rest-mgmt-tm-sys-version.ind 192.168.197.51
#
#   Run a command against 192.168.197.51 in verbose mode
#   crfull rest-mgmt-tm-sys-version.ind 192.168.197.51 verbose
#
#####################################################################################

crfull() {

    if [ -z "$3" ]; then
        command-runner.sh full-command --basic-authentication $USER,$PASSWORD --ssh $USER,$PASSWORD $1 $2
    else
        command-runner.sh full-command --verbose --basic-authentication $USER,$PASSWORD --ssh $USER,$PASSWORD $1 $2
    fi

}

#####################################################################################
#                                   f5full
#
#   Description:
#   Runs a script, or a directory of indeni scripts in full-command mode
#
#   Usage:
#   f5full <script name or directory name> [device ip]
#
#   Examples:
#   Run a command against $DEFAULTF5
#   f5full rest-mgmt-tm-sys-version.ind
#
#   Run a command against 192.168.197.51
#   f5full rest-mgmt-tm-sys-version.ind 192.168.197.51
#
#####################################################################################

f5full() {
    if [ -z "$2" ]; then
        crfull $1 $DEFAULTF5
    else
        crfull $1 $2
    fi
}

#Same as above but in verbose mode
f5fullverbose() {
    if [ -z "$2" ]; then
        crfull $1 $DEFAULTF5 verbose
    else
        crfull $1 $2 verbose
    fi
}

#####################################################################################
#                                   finddoc
#
#   Description:
#   Finds prewritten documentation for a specific metric
#
#   Usage:
#   finddoc <Path to search (recursive)> <metric name>
#
#   Example:
#   This example would find documentation for "network-interface-rx-errors" in "./f5"
#   finddoc ./f5 network-interface-rx-errors
#
#####################################################################################

finddoc() {
    find $1 -name *.ind | xargs sed -n '/COMMENTS/,/^\#\!/p' | sed -n "/^[^ ]*$2.*:/,/^[^ ]/p"
}

#####################################################################################
#                                   crparse
#
#   Description:
#   Takes a script file and runs command-runner in parse-only mode against an input
#   file with the same name
#
#   Usage:
#   crparse <script name> [input file]
#
#   Examples:
#
#   This example would run command-runner in mode parse-only against
#   "rest-mgmt-tm-sys-version.ind" with input file "rest-mgmt-tm-sys-version.input"
#   from either the current working directory or ./test:
#
#   crparse rest-mgmt-tm-sys-version.ind
#
#   This example would run command-runner in mode parse-only against
#   "rest-mgmt-tm-sys-version.ind" with input file "rest-mgmt-tm-sys-version.input"
#   from either the current working directory or ./test:
#
#   crparse rest-mgmt-tm-sys-version.ind rest-mgmt-tm-sys-version.input
#
##################################################################################################################

crparse()
{
    if [ -z "$2" ]; then
        if [ -f "./${1%.*}.input" ]; then
            command-runner.sh parse-only $1 -f "${1%.*}.input"
        elif [ -f "./test/${1%.*}.input" ]; then
            command-runner.sh parse-only $1 -f "./test/${1%.*}.input"
        else
            echo -e "No input files.\n\nTried these locations:\n$(echo ./test/${1%.*}.input)\n$(echo ${1%.*}.input)"
        fi
    else
        command-runner.sh parse-only $1 -f $2
    fi
}

#Same as above, but verbose output
crparseverbose()
{
    if [ -z "$2" ]; then
        if [ -f "./${1%.*}.input" ]; then
            command-runner.sh parse-only --verbose $1 -f "${1%.*}.input"
        elif [ -f "./test/${1%.*}.input" ]; then
            command-runner.sh parse-only --verbose $1 -f "./test/${1%.*}.input"
        else
            echo -e "No input files.\n\nTried these locations:\n$(echo ./test/${1%.*}.input)\n$(echo ${1%.*}.input)"
        fi
    else
        command-runner.sh parse-only $1 -f $2
    fi
}